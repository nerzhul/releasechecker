package internal

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	lm "gitlab.com/nerzhul/libmatterbot"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"net/url"
	"os"
)

type config struct {
	Bot        lm.Config `yaml:"bot"`
	DB         dbConfig  `yaml:"database"`
	Mattermost struct {
		ReleaseAnnouncementsChannel string `yaml:"release-announcements-channel"`
	} `yaml:"mattermost"`
	GithubToken string `yaml:"github-token"`
	Schedule    string `yaml:"schedule"`
}

var gconfig config

func (c *config) loadDefaultConfiguration() bool {
	c.DB.URL = "host=postgres dbname=releasechecker user=releasechecker password=releasechecker"
	c.DB.MaxOpenConns = 10
	c.DB.MaxIdleConns = 5
	if lm.IsInContainer() {
		c.DB.MigrationSource = "file:///var/lib/releasechecker/migrations"
	} else {
		c.DB.MigrationSource = "file:///migrations"
	}

	c.Bot.Mattermost.URL = "http://localhost:8065"
	c.Bot.Mattermost.WebhookID = ""
	c.Bot.Mattermost.Username = "bot"
	c.Mattermost.ReleaseAnnouncementsChannel = "releases-announcements"

	c.GithubToken = ""
	c.Schedule = "*/5 * * * *"

	c.Bot.HTTP.Port = 8080
	c.Bot.HTTP.EnablePProf = false

	return true
}

func (c *config) loadConfigFromEnv() {
	err := envconfig.Process("", c)
	if err != nil {
		log.Fatal(err.Error())
	}

	mattermostKubeURL := os.Getenv("MATTERMOST_PORT")
	if len(mattermostKubeURL) > 0 {
		log.Infof("[k8s discovery] MATTERMOST_PORT url found: %s", mattermostKubeURL)
		mattermostParsedURL, err := url.Parse(mattermostKubeURL)
		if err != nil {
			log.Fatalf("Unable to parse MATTERMOST_PORT URL: %v\n", err)
		}

		if mattermostParsedURL.Scheme == "tcp" {
			mattermostParsedURL.Scheme = "http"
		}

		c.Bot.Mattermost.URL = fmt.Sprintf("%s://%s", mattermostParsedURL.Scheme, mattermostParsedURL.Host)
		log.Infof("Mattermost URL set to: %s", c.Bot.Mattermost.URL)
	}

	log.Infof("Loaded configuration from env vars")
}

func (c *config) loadConfigFromFile(path string) {
	if len(path) == 0 {
		log.Info("Configuration path is empty, using default configuration.")
		return
	}

	log.Infof("Loading configuration from '%s'...", path)

	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Errorf("Failed to read YAML file #%v ", err)
		return
	}

	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Errorf("error: %v", err)
		return
	}

	log.Infof("Configuration loaded from '%s'.", path)
}

func loadConfiguration(path string) {
	gconfig.loadDefaultConfiguration()
	gconfig.loadConfigFromFile(path)
	gconfig.loadConfigFromEnv()

	if len(gconfig.Bot.GetWebhookURL()) == 0 {
		log.Fatalf("configuration error: bot.mattermost.webhook-id is empty.")
	}

	if gconfig.Bot.HTTP.EnablePProf {
		log.Infof("golang pprof listener is enabled.")
	}

	log.Infof("Mattermost webhook URL set to: %s", gconfig.Bot.GetWebhookURL())
}
