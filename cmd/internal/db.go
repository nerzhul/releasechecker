package internal

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"   // golang-migrate requires blank import
	_ "github.com/golang-migrate/migrate/v4/source/github" // golang-migrate requires blank import
	_ "github.com/lib/pq"                                  // pq requires blank import
)

type dbConfig struct {
	URL             string `yaml:"url"`
	MaxIdleConns    int    `yaml:"max-idle-conns"`
	MaxOpenConns    int    `yaml:"max-open-conns"`
	MigrationSource string `yaml:"db-migration-source"`
}

type rcDB struct {
	nativeDB *sql.DB
	config   *dbConfig
}

func (db *rcDB) init() bool {
	if db.config == nil {
		log.Errorf("DB config is nil !!!")
		return false
	}

	log.Infof("Connecting to ReleaseChecker DB at %s", db.config.URL)
	nativeDB, err := sql.Open("postgres", db.config.URL)
	if err != nil {
		log.Errorf("Failed to connect to ReleaseChecker DB: %s", err)
		return false
	}

	db.nativeDB = nativeDB
	if !db.ValidationQuery() {
		db.nativeDB = nil
		return false
	}

	db.nativeDB.SetMaxIdleConns(db.config.MaxIdleConns)
	db.nativeDB.SetMaxOpenConns(db.config.MaxOpenConns)

	if !db.runMigrations() {
		db.nativeDB = nil
		return false
	}

	log.Infof("Connected to ReleaseChecker DB.")
	return true
}

func (db *rcDB) runMigrations() bool {
	log.Infof("Checking for schema migrations to perform...")
	nativeDB, err := sql.Open("postgres", db.config.URL)
	if err != nil {
		log.Errorf("Failed to connect to ReleaseChecker DB while running migrations: %s", err)
		return false
	}

	driver, err := postgres.WithInstance(nativeDB, &postgres.Config{})
	if err != nil {
		log.Errorf("Unable to create migration instance on ReleaseChecker DB: %s", err)
		return false
	}

	m, err := migrate.NewWithDatabaseInstance(
		db.config.MigrationSource,
		"postgres", driver)

	if err != nil {
		log.Errorf("Unable to run migrations on ReleaseChecker DB: %s", err)
		return false
	}

	defer m.Close()

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Errorf("ReleaseChecker DB Migration failed: %s", err)
		return false
	}

	log.Infof("Schema migrations done.")

	return true
}

func (db *rcDB) ValidationQuery() bool {
	rows, err := db.nativeDB.Query(ValidationQuery)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Failed to run ReleaseChecker validation query: %s", err)
		return false
	}

	return true
}

func (db *rcDB) AddGithubRepository(group string, name string) bool {
	_, err := db.nativeDB.Exec(addGithubRepositoryQuery, group, name)
	if err != nil {
		log.Errorf("Unable to add Github repository configuration to DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) RemoveGithubRepository(group string, name string) bool {
	_, err := db.nativeDB.Exec(removeGithubRepositoryQuery, group, name)
	if err != nil {
		log.Errorf("Unable to remove Github repository configuration from DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) GetGithubConfiguredRepositoriesToCheck() ([]githubRepository, error) {
	rows, err := db.nativeDB.Query(getGithubRepositories)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to execute getGithubRepositories: %s", err)
		return nil, err
	}

	var repoList []githubRepository

	for rows.Next() {
		gr := githubRepository{}
		if err := rows.Scan(&gr.group, &gr.name); err != nil {
			log.Errorf("Unable to read getGithubRepositories: %s", err)
			return nil, err
		}

		repoList = append(repoList, gr)
	}

	return repoList, nil
}

func (db *rcDB) SetGithubRepositoryLastUpdate(group string, name string) bool {
	_, err := db.nativeDB.Exec(updateGithubRepositoryLastUpdate, group, name)
	if err != nil {
		log.Errorf("Unable to set Github repository last update DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) RegisterRepositoryTag(group string, name string, tag string) bool {
	_, err := db.nativeDB.Exec(addGithubRepositoryTag, group, name, tag)
	if err != nil {
		log.Errorf("Unable to add Github repository tag to DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) IsGithubRepositoryRegistered(group string, name string) (bool, error) {
	rows, err := db.nativeDB.Query(isGithubRepositoryRegistered, group, name)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to check if Github repository is registered: %s", err)
		return true, err
	}

	repoExists := false
	if rows.Next() {
		if err := rows.Scan(&repoExists); err != nil {
			log.Errorf("Unable to check if Github repository is registered: %s", err)
			return false, err
		}
	}

	return repoExists, nil
}

func (db *rcDB) IsGithubRepositoryTagRegistered(group string, name string, tag string) (bool, error) {
	rows, err := db.nativeDB.Query(isGithubRepositoryTagRegistered, group, name, tag)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to check if Github repository tag is registered: %s", err)
		return true, err
	}

	tagExists := false
	if rows.Next() {
		if err := rows.Scan(&tagExists); err != nil {
			log.Errorf("Unable to check if Github repository tag is registered: %s", err)
			return false, err
		}
	}

	return tagExists, nil
}

// DockerHub

func (db *rcDB) AddDockerHubImage(group string, name string) bool {
	_, err := db.nativeDB.Exec(addDockerHubImageQuery, group, name)
	if err != nil {
		log.Errorf("Unable to add DockerHub image configuration to DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) RemoveDockerHubImage(group string, name string) bool {
	_, err := db.nativeDB.Exec(removeDockerHubImageQuery, group, name)
	if err != nil {
		log.Errorf("Unable to remove DockerHub image configuration from DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) GetDockerHubConfiguredImagesToCheck() ([]dockerHubImage, error) {
	rows, err := db.nativeDB.Query(getDockerHubImages)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to execute getDockerHubImages: %s", err)
		return nil, err
	}

	var imageList []dockerHubImage

	for rows.Next() {
		gr := dockerHubImage{}
		if err := rows.Scan(&gr.group, &gr.name); err != nil {
			log.Errorf("Unable to read getDockerHubImages: %s", err)
			return nil, err
		}

		imageList = append(imageList, gr)
	}

	return imageList, nil
}

func (db *rcDB) SetLastDockerhubImageUpdate(group string, name string) bool {
	_, err := db.nativeDB.Exec(updateDockerHubImageLastUpdate, group, name)
	if err != nil {
		log.Errorf("Unable to set DockerHub image last update DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) RegisterDockerHubImageTag(group string, name string, tag string) bool {
	_, err := db.nativeDB.Exec(addDockerHubImageTag, group, name, tag)
	if err != nil {
		log.Errorf("Unable to add DockerHub image tag to DB: %s", err)
		return false
	}

	return true
}

func (db *rcDB) IsDockerHubImageRegistered(group string, name string) (bool, error) {
	rows, err := db.nativeDB.Query(isDockerHubImageRegistered, group, name)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to check if DockerHub image is registered: %s", err)
		return true, err
	}

	imageExists := false
	if rows.Next() {
		if err := rows.Scan(&imageExists); err != nil {
			log.Errorf("Unable to check if DockerHub image is registered: %s", err)
			return false, err
		}
	}

	return imageExists, nil
}

func (db *rcDB) IsDockerHubImageTagRegistered(group string, name string, tag string) (bool, error) {
	rows, err := db.nativeDB.Query(isDockerHubImageTagRegistered, group, name, tag)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		log.Errorf("Unable to check if DockerHub image tag is registered: %s", err)
		return true, err
	}

	tagExists := false
	if rows.Next() {
		if err := rows.Scan(&tagExists); err != nil {
			log.Errorf("Unable to check if DockerHub image tag is registered: %s", err)
			return false, err
		}
	}

	return tagExists, nil
}
