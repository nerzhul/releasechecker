package internal

import (
	"context"
	"fmt"
	"github.com/google/go-github/v47/github"
	"golang.org/x/oauth2"
	"net/http"
)

type githubRepository struct {
	group string
	name  string
}

// GithubClient github client
type GithubClient struct {
	context context.Context
	github  *github.Client
}

// NewGithubClient Create github client
func NewGithubClient() *GithubClient {
	c := &GithubClient{}
	var httpClient *http.Client

	c.context = context.Background()

	// If there is a token defined, use it
	if len(gconfig.GithubToken) > 0 {
		ts := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: gconfig.GithubToken},
		)
		httpClient = oauth2.NewClient(c.context, ts)
	}

	c.github = github.NewClient(httpClient)

	return c
}

func (c *GithubClient) checkGithubNewTags(b *Bot) bool {
	log.Infof("Checking new Github tags...")

	repositories, err := gDB.GetGithubConfiguredRepositoriesToCheck()
	if err != nil {
		log.Errorf("Failed to fetch Github configured repositories")
		return false
	}

	for _, repo := range repositories {
		log.Infof("Fetching tags for github.com:%s/%s.git", repo.group, repo.name)

		tags, _, err := c.github.Repositories.ListTags(c.context, repo.group, repo.name, nil)
		if err != nil {
			log.Errorf("Failed to list Github repository tags: %s", err)
			continue
		}

		rb := NewReleaseStringBuilder(
			fmt.Sprintf("**[%s/%s](https://github.com/%s/%s/)** git tags",
				repo.group, repo.name, repo.group, repo.name),
		)

		newTags := make([]string, 0)
		for _, t := range tags {
			registered, err := gDB.IsGithubRepositoryTagRegistered(repo.group, repo.name, *t.Name)
			if err != nil {
				log.Errorf("Failed to verify if Github repository tag is registered: %s", err)
				continue
			}

			if !registered {
				newTags = append(newTags, *t.Name)
				rb.Append(fmt.Sprintf("* [%s](https://github.com/%s/%s/releases/tag/%s)",
					*t.Name, repo.group, repo.name, *t.Name))
			}
		}

		if !rb.Empty() {
			// Unable to dispatch event, aborting
			if !b.DispatchEvent(rb.String()) {
				return false
			}

			for _, t := range newTags {
				if !gDB.RegisterRepositoryTag(repo.group, repo.name, t) {
					log.Errorf("Failed to register Github repository tag: %s", err)
					continue
				}
			}
		}

		gDB.SetGithubRepositoryLastUpdate(repo.group, repo.name)
	}

	log.Infof("New Github tags fetch done.")
	return true
}
