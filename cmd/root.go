package releasechecker

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nerzhul/releasechecker/cmd/internal"
)

var configFile = ""

func init() {
	rootCmd.PersistentFlags().StringVar(&configFile, "config", "releasechecker.yaml", "config file")
}

var rootCmd = &cobra.Command{
	Use:   "releasechecker",
	Short: "releasechecker notify about Dockerhub and Github releases",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		internal.StartApp(configFile)
	},
}

// Execute root command execution
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
