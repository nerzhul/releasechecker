FROM golang:1.20.1

COPY . /src/
WORKDIR /src/
RUN export GIT_COMMIT=$(git rev-list -1 HEAD) && \
	env CGO_ENABLED=0 \
		go build \
		-ldflags "-X gitlab.com/nerzhul/releasechecker/internal.AppVersion=$GIT_COMMIT" \
		-o "/artifacts/releasechecker" main.go

FROM alpine:3.17.2
RUN addgroup -g 778 -S releasechecker && adduser -u 778 -D -S -G releasechecker releasechecker
USER releasechecker
WORKDIR /var/lib/releasechecker
COPY --from=0 /artifacts/releasechecker /usr/bin/releasechecker
COPY --from=0 /src/cmd/res/migrations /var/lib/releasechecker/migrations
COPY --from=0 /etc/ssl/certs /etc/ssl/certs
COPY --from=0 /usr/share/ca-certificates /usr/share/ca-certificates

CMD ["/usr/bin/releasechecker", "--config=/etc/bot/releasechecker.yml"]
